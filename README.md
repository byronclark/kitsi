# kitsi

Examples for [libjitsi](https://jitsi.org/Projects/LibJitsi) implemented in [kotlin](http://kotlinlang.org/).

The idea is to show how to use libjitsi from kotlin in a [gradle](http://gradle.org/) project. It's also the first kotlin code I've ever written so I'd love critiques and suggestions for improvement.