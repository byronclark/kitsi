package kitsi.receive

import kitsi.common.AVExampleBase
import kitsi.common.CommandLineOptions
import org.jitsi.service.libjitsi.LibJitsi
import org.jitsi.service.neomedia.MediaDirection
import org.kohsuke.args4j.CmdLineException
import org.kohsuke.args4j.CmdLineParser

class AVReceive2(localPortBase: Int, remoteHost: String, remotePortBase: Int) :
        AVExampleBase(localPortBase, remoteHost, remotePortBase) {

    fun start() {
        super.start(MediaDirection.RECVONLY)
    }
}

fun main(args: Array<String>) {
    val commandLine = CommandLineOptions()
    val parser = CmdLineParser(commandLine)
    try {
        parser.parseArgument(args.toList())
    } catch (err: CmdLineException) {
        System.err.println("Error: ${err.message}")
        parser.printUsage(System.err)
        System.exit(1)
    }

    LibJitsi.start()

    val receiver = AVReceive2(commandLine.localPortBase, commandLine.remoteHost, commandLine.remotePortBase)
    receiver.start()
    println("Receiving from ${commandLine.remoteHost}:${commandLine.remotePortBase} on :${commandLine.localPortBase} for 120 seconds...")
    try {
        Thread.sleep(120000)
    } finally {
        receiver.close()
        println("...receiver closed.")
    }

    LibJitsi.stop()
}