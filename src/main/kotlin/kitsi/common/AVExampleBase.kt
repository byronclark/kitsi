package kitsi.common

import org.jitsi.service.libjitsi.LibJitsi
import org.jitsi.service.neomedia.*
import org.jitsi.service.neomedia.format.MediaFormatFactory
import java.net.DatagramSocket
import java.net.InetAddress
import java.net.InetSocketAddress
import java.util.*

open class AVExampleBase(localPortBase: Int, remoteHost: String, remotePortBase: Int) {
    data class PortPair(val rtpPort: Int, val rtcpPort: Int)
    data class StreamConfiguration(val encoding: String?, val clockRate: Double, val rtpPayloadType: Byte)

    protected final val desiredMediaTypes = arrayOf(MediaType.AUDIO, MediaType.VIDEO)

    protected val localPorts = PortPair(localPortBase, localPortBase + 1)
    protected val remoteAddr = InetAddress.getByName(remoteHost)
    protected val remotePorts = PortPair(remotePortBase, remotePortBase + 1)

    protected val mediaStreams = HashMap<MediaType, MediaStream>()

    fun start(direction: MediaDirection) {
        val mediaService = LibJitsi.getMediaService()

        for (mediaType in desiredMediaTypes) {
            val device = mediaService.getDefaultDevice(mediaType, MediaUseCase.CALL);
            val stream = mediaService.createMediaStream(device)
            stream.direction = direction

            val config = when (mediaType) {
                MediaType.AUDIO -> StreamConfiguration("PCMU", 8000.0, -1)
                MediaType.VIDEO -> StreamConfiguration("H264", MediaFormatFactory.CLOCK_RATE_NOT_SPECIFIED, 99)
                else -> StreamConfiguration(null, MediaFormatFactory.CLOCK_RATE_NOT_SPECIFIED, -1)
            }

            if (config.encoding != null) {
                val format = mediaService.formatFactory.createMediaFormat(config.encoding, config.clockRate);
                if (config.rtpPayloadType != (-1).toByte()) {
                    stream.addDynamicRTPPayloadType(config.rtpPayloadType, format)
                }
                stream.format = format;
            }

            val localRTPSocket = DatagramSocket(null)
            localRTPSocket.reuseAddress = true
            localRTPSocket.bind(InetSocketAddress(localPorts.rtpPort))

            val localRTCPSocket = DatagramSocket(null)
            localRTCPSocket.reuseAddress = true
            localRTCPSocket.bind(InetSocketAddress(localPorts.rtcpPort))

            stream.setConnector(DefaultStreamConnector(localRTPSocket, localRTCPSocket))

            stream.target = MediaStreamTarget(InetSocketAddress(remoteAddr, remotePorts.rtpPort),
                    InetSocketAddress(remoteAddr, remotePorts.rtcpPort))
            stream.name = mediaType.toString()
            mediaStreams.put(mediaType, stream)
        }

        for (stream in mediaStreams.values) {
            stream.start();
        }
    }

    fun close() {
        for (stream in mediaStreams.values) {
            try {
                stream.stop()
            } finally {
                stream.close()
            }
        }

        mediaStreams.clear()
    }
}