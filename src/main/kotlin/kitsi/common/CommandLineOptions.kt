package kitsi.common

import org.kohsuke.args4j.Option

class CommandLineOptions {
    @Option(name = "-local-port-base", metaVar = "<port>", usage = "Local listening port base. Defaults to 4000")
    var localPortBase = 4000

    @Option(name = "-remote-port-base", metaVar = "<port>", usage = "Remote destination port base. Defaults to 5000")
    var remotePortBase = 5000

    @Option(name = "-remote-host", metaVar = "<hostname>", usage = "Remote destination hostname. Defaults to 'localhost'")
    var remoteHost = "localhost"
}