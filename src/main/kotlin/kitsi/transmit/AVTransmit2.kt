package kitsi.transmit

import kitsi.common.AVExampleBase
import kitsi.common.CommandLineOptions
import org.jitsi.service.libjitsi.LibJitsi
import org.jitsi.service.neomedia.MediaDirection
import org.kohsuke.args4j.CmdLineException
import org.kohsuke.args4j.CmdLineParser

class AVTransmit2(localPortBase: Int, remoteHost: String, remotePortBase: Int) :
        AVExampleBase(localPortBase, remoteHost, remotePortBase) {

    fun start() {
        super.start(MediaDirection.SENDONLY)
    }
}

fun main(args: Array<String>) {
    val commandLine = CommandLineOptions()
    val parser = CmdLineParser(commandLine)
    try {
        parser.parseArgument(args.toList())
    } catch (err: CmdLineException) {
        System.err.println("Error: ${err.message}")
        parser.printUsage(System.err)
        System.exit(1)
    }

    LibJitsi.start()

    val transmitter = AVTransmit2(commandLine.localPortBase, commandLine.remoteHost, commandLine.remotePortBase)
    transmitter.start()
    println("Transmitting to ${commandLine.remoteHost}:${commandLine.remotePortBase} on :${commandLine.localPortBase} for 60 seconds...")
    try {
        Thread.sleep(60000)
    } finally {
        transmitter.close()
        println("...transmission ended.")
    }

    LibJitsi.stop()
}